from random import randint
input('Hi, what is your name?\n')
guesses = 0

#Guess loop
for n in range(0,5):
    guesses += 1
    print(f'Guess {guesses}: Were you born in: {randint(1,12)} / {randint(1900,2023)}')
    userinput  = input('yes or no? ')
    if userinput == 'no':
        if not guesses == 5:
            print('Drat! Let me try again')
        else:
            print('I have no other things to do now. Good bye')
    elif userinput == 'yes':
        print('I knew it!')
        break
    else:
        print('Sorry, that input is incorrect, please type "yes" or "no"')
        pass
