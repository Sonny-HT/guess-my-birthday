from random import randint
input('Hi, what is your name?\n')

#Guess 1
print(f'Guess 1: Were you born in: {randint(1,12)} / {randint(1900,2023)}')
answer = input ('yes or no?')
if answer == 'no':
    print('Drat! let me try again!')
elif answer == 'yes':
    print('I knew it!')
    exit()
else:
    print('Invalid input, please enter "yes" or "no"')

#Guess 2
print(f'Guess 2: Were you born in: {randint(1,12)} / {randint(1900,2023)}')
answer = input ('yes or no?')
if answer == 'no':
    print('Drat! let me try again!')
elif answer == 'yes':
    print('I knew it!')
    exit()
else:
    print('Invalid input, please enter "yes" or "no"')

#Guess 3
print(f'Guess 3: Were you born in: {randint(1,12)} / {randint(1900,2023)}')
answer = input ('yes or no?')
if answer == 'no':
    print('Drat! let me try again!')
elif answer == 'yes':
    print('I knew it!')
    exit()
else:
    print('Invalid input, please enter "yes" or "no"')

#Guess 4
print(f'Guess 4: Were you born in: {randint(1,12)} / {randint(1900,2023)}')
answer = input ('yes or no?')
if answer == 'no':
    print('Drat! let me try again!')
elif answer == 'yes':
    print('I knew it!')
    exit()
else:
    print('Invalid input, please enter "yes" or "no"')

#Guess 5
print(f'Guess 5: Were you born in: {randint(1,12)} / {randint(1900,2023)}')
answer = input ('yes or no?')
if answer == 'no':
    print('I have no other things to do now. Good bye.')
elif answer == 'yes':
    print('I knew it!')
    exit()
else:
    print('Invalid input, please enter "yes" or "no"')
