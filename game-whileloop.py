from random import randint
guesses = 0
answer = False
input('Hi, what is your name?\n')

while answer == False:
    guesses += 1
    print(f'Guess #{guesses} Were you born in: {randint(1,12)} / {randint(1900,2023)} ')
    userinput = input('yes or no? ')
    if userinput == 'no':
        if not guesses == 5:
            print('Drat! Let me try again')
        else:
            print('I have no other things to do now. Good bye')
            break
    elif userinput == 'yes':
        print('I knew it!')
        answer = True
    else:
        print('Sorry, that input is incorrect, please type "yes" or "no"')
        pass
